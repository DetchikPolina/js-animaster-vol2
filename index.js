(function main() {
    document.getElementById('ourAnimationPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('ourAnimationBlock');
            const animation = Animaster().ourAnimation(block);
            function forFadeInAnimation() {
                animation.reset(block);
                document.getElementById('ourAnimationReset')
                    .removeEventListener('click', forFadeInAnimation);
            }
            document.getElementById('ourAnimationReset')
                .addEventListener('click', forFadeInAnimation);
        });
    document.getElementById('fadeInPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('fadeInBlock');
            const animation = Animaster().fadeIn(block, 5000);
            function forFadeInAnimation() {
                animation.reset(block);
                document.getElementById('fadeInReset')
                    .removeEventListener('click', forFadeInAnimation);
            }
            document.getElementById('fadeInReset')
                .addEventListener('click', forFadeInAnimation);
        });
    document.getElementById('fadeOutPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('fadeOutBlock');
            const animation = Animaster().fadeOut(block, 5000);
            function forFadeOutAnimation() {
                animation.reset(block);
                document.getElementById('fadeOutReset')
                    .removeEventListener('click', forFadeOutAnimation);
            }
            document.getElementById('fadeOutReset')
                .addEventListener('click', forFadeOutAnimation);
        });
    document.getElementById('showAndHidePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('showAndHideBlock');
            const animation = Animaster().showAndHide(block, 5000);
            function forShowAndHideAnimation() {
                animation.reset(block, false);
                document.getElementById('showAndHideReset')
                    .removeEventListener('click', forShowAndHideAnimation);
            }
            document.getElementById('showAndHideReset')
                .addEventListener('click', forShowAndHideAnimation);
        });
    document.getElementById('movePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('moveBlock');
            const movePlayAnimation = Animaster().move(block, 1000, {
                x: 100,
                y: 10
            });
            function forMoveAnimation() {
                movePlayAnimation.reset(block);
                document.getElementById('moveReset')
                    .removeEventListener('click', forMoveAnimation);
            }
            document.getElementById('moveReset')
                .addEventListener('click', forMoveAnimation);
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('scaleBlock');
            const scalePlayAnimation = Animaster().scale(block, 1000, 1.25);

            function forScaleAnimation() {
                scalePlayAnimation.reset(block);
                document.getElementById('scaleReset')
                    .removeEventListener('click', forScaleAnimation);
            }
            document.getElementById('scaleReset')
                .addEventListener('click', forScaleAnimation);
        });

    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('moveAndHideBlock');
            const moveAndHideAnimation = Animaster().moveAndHide(block, 2000, {
                x: 100,
                y: 20
            });
            function forMoveAndHideAnimation() {
                moveAndHideAnimation.reset(block);
                document.getElementById('moveAndHideReset')
                    .removeEventListener('click', forMoveAndHideAnimation);
            }
            document.getElementById('moveAndHideReset')
                .addEventListener('click', forMoveAndHideAnimation);
        });

    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('heartBeatingBlock');
            const heartBeatingAnimation = Animaster().heartBeating(block, true)
            function HeartBeatingStop(){
                heartBeatingAnimation.stop(block);
                document.getElementById('heartBeatingStop')
                    .removeEventListener('click', HeartBeatingStop);
            }
            function HeartBeatingReset(){
                heartBeatingAnimation.reset(block);
                document.getElementById('heartBeatingReset')
                    .removeEventListener('click', HeartBeatingReset);
            }
            document.getElementById('heartBeatingStop')
                .addEventListener('click', HeartBeatingStop);
            document.getElementById('heartBeatingReset')
                .addEventListener('click', HeartBeatingReset);
        });
    document.getElementById('customPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('customBlock');
            const block1 = document.getElementById('customBlock1');
            const customAnimation = Animaster()
                .addMove(200, {x: 40, y: 0})
                .addScale(800, 0.5)
                .addMove(200, {x: 0, y: 40})
                .addScale(800, 1)
                .addMove(200, {x: 40, y: 0})
                .addScale(800, 0.5)
                .addMove(200, {x: 0, y: 40})
                .addScale(800, 1)
               // .play(block);
                //play(block1);
            customAnimation.play(block);
            customAnimation.play(block1);

            function forAnimation() {
                customAnimation
                    .play(block)
                    .reset(block);
                customAnimation
                    .play(block1)
                    .reset(block1);
                document.getElementById('customReset')
                    .removeEventListener('click', forAnimation);
            }
            document.getElementById('customReset')
                .addEventListener('click', forAnimation);
        });
    const handler = new Animaster()
        .addMove(200, {x: 80, y: 0})
        .addMove(200, {x: 0, y: 0})
        .addMove(200, {x: 80, y: 0})
        .addMove(200, {x: 0, y: 0})
        .buildHandler(false);
    document
        .getElementById("handlerBlock")
        .addEventListener("click", handler);



    document.getElementById('shakingPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('shakingBlock');
            const shakingPlayBlock = Animaster().shaking(block, 1000, 1.25);
            function shakingStop(){
                shakingPlayBlock.stop(block);
                document.getElementById('shakingStop')
                    .removeEventListener('click', shakingStop);
            }
            function shakingReset(){
                shakingPlayBlock.reset(block);
                document.getElementById('shakingStop')
                    .removeEventListener('click', shakingReset);
            }

            document.getElementById('shakingStop')
                .addEventListener('click', shakingStop);
            document.getElementById('shakingReset')
                .addEventListener('click', shakingReset);
        });
})();

function Animaster() {
    function resetMoveAndScale(element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    }
    function resetFadeIn(element) {
        element.style.transitionDuration = null;
        element.classList.remove("show");
        element.classList.add("hide");
    }
    function resetFadeOut(element) {
        element.style.transitionDuration = null;
        element.classList.remove("hide");
        element.classList.add("show");
    }
    function resetRotation(element) {
        element.style.transitionDuration = null;
        element.style.transform = getTransform(null, null, 0);
    }
    return {
        fadeIn: function(element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('hide');
            element.classList.add('show');
            return {
                reset: function (element) {
                    resetFadeIn(element);
                }
            };
        },

        fadeOut: function(element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('show');
            element.classList.add('hide');
            return {
                reset: function (element) {
                    resetFadeOut(element);
                }
            };
        },

        rotate: function(element, duration, rotation, ratio) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(null, ratio, rotation);
        },

        ourAnimation: function(element) {
            return this
                .addRotate(500, 360, 0.1)
                .addRotate(500, -360, 1)
                .play(element);
        },

        move: function(element, duration, translation) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(translation, null);
            return {
                reset: function (element) {
                    resetMoveAndScale(element);
                }
            };
        },
        scale: function(element, duration, ratio) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(null, ratio);
            return {
                reset: function (element) {
                    resetMoveAndScale(element);
                }
            };
        },

        moveAndHide: function(element, duration, translation) {
            return this
                .addMove(duration * 0.4, translation)
                .addFadeOut(duration * 0.6)
                .play(element);
        },
        showAndHide: function(element, duration) {
            return this
                .addFadeIn(duration / 3)
                .addDelay(duration / 3)
                .addFadeOut(duration / 3)
                .play(element);

        },
        shaking: function(element) {
            return this
                .addMove(500, { x: 40, y: 0 })
                .addMove(500, { x: 0, y: 0 })
                .play(element, true);
        },
        heartBeating: function(element) {
            return this
                .addScale(500, 1.4)
                .addScale(500, 1)
                .play(element, true);
        },
        delay: function (element, duration) {
            setTimeout(() => {}, duration);
            return this
        },
        _steps: [],
        addMove: function(duration, translation) {
            this._steps.push({
                name: 'move',
                duration: duration,
                translation: translation
            })
            return this
        },

        addScale: function(duration, ratio) {
            this._steps.push({
                name: 'scale',
                duration: duration,
                ratio: ratio
            })
            return this
        },

        addRotate: function(duration, rotation, ratio){
            this._steps.push({
                name: 'rotate',
                rotation: rotation,
                duration: duration,
                ratio: ratio,
            })
            return this
        },

        addFadeIn: function(duration) {
            this._steps.push({
                name: 'fadeIn',
                duration: duration,
            })
            return this
        },

        addFadeOut: function(duration) {
            this._steps.push({
                name: 'fadeOut',
                duration: duration,
            })
            return this
        },

        addDelay(duration) {
            this._steps.push({
                name: 'delay',
                duration: duration,
            })
            return this
        },
        buildHandler: function (cycled){
            let that = this
            return function () {
                that.play(this, cycled)
            }
        },

        play: function (element, cycled = false) {
            let step = 0
            let animInterval = setInterval(() => {
                if (step === this._steps.length && cycled === true) {
                    step = 0
                }
                if (!this._steps[step] && cycled === false) {
                    return
                }
                console.log(this._steps[step].name, step)
                console.log(this._steps[step].duration)

                let animation = this._steps[step]

                switch  (this._steps[step].name) {
                    case "move":
                        Animaster().move(element, animation.duration, animation.translation)
                        break
                    case "scale":
                        Animaster().scale(element, animation.duration, animation.ratio)
                        break
                    case "rotate":
                        Animaster().rotate(element, animation.duration, animation.rotation, animation.ratio)
                        break
                    case "fadeIn":
                        Animaster().fadeIn(element, animation.duration)
                        break
                    case "fadeOut":
                        Animaster().fadeOut(element,  animation.duration)
                        break
                    case "moveAndHide":
                        Animaster().moveAndHide(element,  animation.duration, animation.translation)
                        break
                    case "showAndHide":
                        Animaster().showAndHide(element,  animation.duration)
                        break
                    case "heartBeating":
                        Animaster().heartBeating(element, animation.duration)
                        break
                    case "delay":
                        Animaster().delay(element, animation.duration)
                        break
                }
                step++;
            }, this._steps[step + 1].duration)
            return {
                stop: function(){
                    clearInterval(animInterval)
                    console.log('sda')
                },
                reset: function (element, flag = true) {
                    clearInterval(animInterval);
                    resetRotation(element);
                    resetMoveAndScale(element);
                    resetFadeIn(element);
                    if (flag)
                        resetFadeOut(element);
                }
            }

        },
    }
}

function getTransform(translation, ratio, rotation = null) {
    const result = [];
    if (translation)
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    if (ratio)
        result.push(`scale(${ratio})`);
    if (rotation)
        result.push(`rotate(${rotation}deg)`);
    return result.join(' ');
}
